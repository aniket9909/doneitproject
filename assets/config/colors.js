import React from "react";

export const colors = {
  primary: "#d65c5c",
  sucess: "#42f58d",
  secondary: "#42a1f5",
  lightgrey: "#f8f4f4",
  danger: "#ff5656",
  white: "#fff",
};
