import React from "react";
import { View, StyleSheet } from "react-native";
import { colors } from "../config/colors";
export const ListSaperatore = () => {
  return <View style={styles.separet} />;
};
const styles = StyleSheet.create({
  separet: {
    width: "100%",
    height: 1,
    backgroundColor: colors.lightgrey,
  },
});
