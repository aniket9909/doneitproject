import React from "react";
import { Text, StyleSheet } from "react-native";

export const Apptext = ({ text }) => {
  return <Text style={styles.text}>{text}</Text>;
};
const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    fontWeight: "400",
    marginVertical: 5,
  },
});
