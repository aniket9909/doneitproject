import React from "react";
import {
  Image,
  View,
  StyleSheet,
  TouchableHighlight,
} from "react-native";
import { GestureHandlerRootView } from "react-native-gesture-handler";
import Swipeable from "react-native-gesture-handler/Swipeable";

import { colors } from "../config/colors";
import { Apptext } from "./Apptext";
import { ListItemDelete } from "./ListItemDelete";

export const ListItems = ({
  title,
  subtitle,
  image,
  onpress,
  color,
  renderRightActions,
}) => {
  return (
    <GestureHandlerRootView children={Swipeable}>
      <Swipeable renderRightActions={renderRightActions}>
        <TouchableHighlight
          underlayColor={colors.lightgrey}
          onPress={onpress}
        >
          <View style={styles.listcontainer}>
            <Image
              style={styles.listimage}
              source={image}
            />
            <View style={styles.user}>
              <Apptext
                style={styles.title}
                text={title}
              ></Apptext>
              <Apptext
                style={styles.subtitle}
                text={subtitle}
              ></Apptext>
            </View>
          </View>
        </TouchableHighlight>
      </Swipeable>
    </GestureHandlerRootView>
  );
};

const styles = StyleSheet.create({
  listcontainer: {
    padding: 10,
    flexDirection: "row",
    backgroundColor: "white",
  },
  listimage: {
    borderRadius: 35,
    width: 70,
    height: 70,
  },
  user: {
    flexDirection: "column",
    marginHorizontal: 10,
  },
  subtitle: {
    color: "red",
  },
});
