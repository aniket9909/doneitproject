import React from "react";
import { StyleSheet } from "react-native";
import { colors } from "../../config/colors";

export const styles = StyleSheet.create({
  block: {
    flex: 1,
    width: "100%",
    height: "50%",
    backgroundColor: "black",
    flexDirection: "column",
    justifyContent: "center",
  },
  innerblock: {
    backgroundColor: colors.green,
    width: 100,
    height: 100,
  },
  image: {
    width: 450,
    height: 600,
    resizeMode: "cover",
    justifyContent: "center",
  },
  iconBlock: {
    flexDirection: "row",
  },
  crossicon: {
    color: "white",
    justifyContent: "flex-start",
    top: -30,
  },
  deleteicon: {
    color: "white",
    justifyContent: "flex-end",
    right: -300,
    top: -30,
  },
});
