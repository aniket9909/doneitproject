import React from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
} from "react-native";

import { colors } from "../config/colors";

export const AppButton = ({
  title,
  onPress,
  color = "primary",
}) => {
  return (
    <TouchableOpacity
      style={[
        styles.button,
        { backgroundColor: colors[color] },
      ]}
      onPress={onPress}
    >
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    width: "100%",
    height: 50,
    backgroundColor: colors.primary,
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    marginVertical: 8,
  },
  text: {
    color: "white",
    fontWeight: "bold",
    fontSize: 22,
  },
});
