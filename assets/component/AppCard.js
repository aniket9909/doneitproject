import React from "react";
import {
  Text,
  View,
  Image,
  StyleSheet,
} from "react-native";

import { colors } from "../config/colors";

export const AppCard = ({ title, subtitle, image }) => {
  return (
    <View style={styles.card}>
      <Image
        style={styles.cardimage}
        source={image}
      ></Image>
      <View style={styles.cardinfo}>
        <Text style={styles.cardtitle}>{title}</Text>
        <Text style={styles.cardsub}>{subtitle}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    overflow: "hidden",
    borderRadius: 20,
    backgroundColor: "white",
    marginVertical: 20,
  },
  cardimage: {
    width: "100%",
    height: 200,
  },
  cardtitle: {
    marginBottom: 5,
    fontSize: 25,
    fontWeight: "bold",
  },
  cardinfo: {
    padding: 20,
  },
  cardsub: {
    color: colors.sucess,
  },
});
