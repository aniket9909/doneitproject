import React from "react";
import { View, StyleSheet } from "react-native";
import { FontAwesome } from "@expo/vector-icons";

import { colors } from "../config/colors";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";

export const ListItemDelete = ({ onpress }) => {
  return (
    <TouchableWithoutFeedback onPress={onpress}>
      <View style={styles.gesture}>
        <FontAwesome
          name="trash"
          size={40}
          color={colors.white}
          style={styles.trashicon}
        ></FontAwesome>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  gesture: {
    width: 70,
    height: 90,
    backgroundColor: colors.danger,
    alignItems: "center",
    justifyContent: "center",
  },
});
