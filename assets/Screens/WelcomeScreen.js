import React from "react";
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  View,
} from "react-native";

import { AppButton } from "../component/AppButton";
import Apptext from "../component/Apptext";

export const WelcomeScreen = () => {
  return (
    <ImageBackground
      style={styles.background}
      blurRadius={5}
      source={require("../chair.jpg")}
    >
      <View style={styles.logoContainer}>
        <Image
          style={styles.logo}
          source={require("../logo-red.png")}
        />
        <Text style={styles.tagline}>
          Sell what you want !!{" "}
        </Text>
      </View>
      <View style={styles.buttonbox}>
        <AppButton
          title="LogIn"
          color="primary"
          style={styles.login}
        />
        <AppButton
          title="Register"
          style={styles.register}
          color="sucess"
        />
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
  },

  tagline: {
    fontSize: 20,
    fontWeight: "500",
  },
  buttonbox: {
    alignItems: "center",
    top: 500,
    marginHorizontal: 30,
  },
  logo: {
    width: 100,
    height: 100,
  },
  logoContainer: {
    alignItems: "center",
    top: 120,
  },
});
