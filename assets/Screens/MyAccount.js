import React from "react";
import {
  Text,
  View,
  StyleSheet,
  Platform,
  StatusBar,
} from "react-native";
import { ListItems } from "../component/ListItems";
import { colors } from "../config/colors";

export const MyAccount = () => {
  return (
    <View style={styles.profile}>
      <ListItems
        title="hii"
        subtitle="aniet@gmail.com"
        image={require("../../assets/mosh.jpg")}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  profile: {
    flex: 1,
    backgroundColor: colors.lightgrey,
    paddingTop:
      Platform.OS === "android"
        ? StatusBar.currentHeight
        : 0,
  },
});
