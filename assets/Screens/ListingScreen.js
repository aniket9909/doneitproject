import React from "react";
import { View, StyleSheet } from "react-native";

import { AppCard } from "../component/AppCard";

export const ListingScreen = () => {
  return (
    <View style={styles.card}>
      <AppCard
        title="jacket"
        subtitle="$10"
        image={require("../../assets/jacket.jpg")}
      />
      <AppCard
        title="jacket"
        subtitle="$10"
        image={require("../../assets/jacket.jpg")}
      />
      <AppCard
        title="jacket"
        subtitle="$10"
        image={require("../../assets/jacket.jpg")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    flex: 1,
    padding: 10,
    paddingVertical: 20,
    backgroundColor: "#f8f4f4",
  },
});
