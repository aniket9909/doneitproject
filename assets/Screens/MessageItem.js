import React, { useState } from "react";
import {
  FlatList,
  Text,
  View,
  Platform,
  StatusBar,
  StyleSheet,
} from "react-native";
import { ListItemDelete } from "../component/ListItemDelete";

import { ListItems } from "../component/ListItems";
import { ListSaperatore } from "../component/ListSaperatore";

const initialMessage = [
  {
    id: 1,
    title: "aniket1",
    description: "t1",
    image: require("../../assets/mosh.jpg"),
  },
  {
    id: 2,
    title: "deed",
    description: "t2",
    image: require("../../assets/mosh.jpg"),
  },
  {
    id: 3,
    title: "deed",
    description: "t2",
    image: require("../../assets/mosh.jpg"),
  },
  {
    id: 4,
    title: "deed",
    description: "t2",
    image: require("../../assets/mosh.jpg"),
  },
];

export const MessageItem = () => {
  const [messages, setMessage] = useState(initialMessage);
  const handleDelete = (message) => {
    //delete the message
    const newMessages = messages.filter(
      (m) => m.id !== message.id
    );
    setMessage(newMessages);
  };

  return (
    <View style={styles.screen}>
      <FlatList
        data={messages}
        keyExtractor={(message) => message.id}
        renderItem={({ item }) => (
          <ListItems
            title={item.title}
            subtitle={item.description}
            image={item.image}
            onpress={() =>
              console.log("message selected", item)
            }
            renderRightActions={() => (
              <ListItemDelete
                onpress={() => handleDelete(item)}
              />
            )}
          />
        )}
        ItemSeparatorComponent={ListSaperatore}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    paddingTop:
      Platform.OS === "android"
        ? StatusBar.currentHeight
        : 0,
  },
});
