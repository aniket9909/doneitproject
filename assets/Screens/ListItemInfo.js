import React from "react";
import { Image, View, StyleSheet } from "react-native";

import { Apptext } from "../component/Apptext";
import { ListItems } from "../component/ListItems";
import { colors } from "../config/colors";

export const ListItemInfo = () => {
  return (
    <View>
      <Image
        style={styles.cardinfoimage}
        source={require("../../assets/jacket.jpg")}
      ></Image>
      <View style={styles.cardinfo}>
        <Apptext text="jacket" />
        <Apptext style={styles.subtitle} text="$ 5" />
      </View>
      <View>
        <ListItems
          title="aniket"
          subtitle="5 followers"
          image={require("../../assets/mosh.jpg")}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cardinfoimage: {
    width: "100%",
    height: 300,
  },
  cardinfo: {
    padding: 20,
  },
  subtitle: {
    color: "green",
  },
});
