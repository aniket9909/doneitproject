import React from "react";
import { StyleSheet, Image, View } from "react-native";
import { styles } from "../component/styles/style";
import { Entypo, MaterialIcons } from "@expo/vector-icons";
export const ViewImage = () => {
  return (
    <View style={styles.block}>
      <View style={styles.iconBlock}>
        <Entypo
          name="cross"
          size={50}
          style={styles.crossicon}
        />
        <MaterialIcons
          name="delete"
          size={50}
          style={styles.deleteicon}
        />
      </View>
      <Image
        style={styles.image}
        source={require("../chair.jpg")}
      ></Image>
    </View>
  );
};
