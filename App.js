import { StatusBar } from "expo-status-bar";
import { View, Image, StyleSheet } from "react-native";

import { AppCard } from "./assets/component/AppCard";
import { MyAccount } from "./assets/Screens/MyAccount";
import { ListItemInfo } from "./assets/Screens/ListItemInfo";
import { MessageItem } from "./assets/Screens/MessageItem";
import { ViewImage } from "./assets/Screens/ViewImage";

export default function App() {
  // return <MessageItem />;
  return <MyAccount />;
}
